# chapter_reader

#### 介绍
IDEA 摸鱼插件 在线小说阅读插件

详见：https://blog.csdn.net/com_study/article/details/115709454

添加配置，方便起见，从config最上方开始添加，并提交Pull Request


#### 安装教程
IDEA插件市场安装

https://plugins.jetbrains.com/plugin/16544-chapter-reader

#### 使用说明
详见：https://blog.csdn.net/com_study/article/details/115709454
